public class S2 {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        StrThread StrThread1 = new StrThread("StrThread1");
        StrThread1.start();
        StrThread StrThread2 = new StrThread("StrThread2");
        StrThread2.start();
    }
}

class StrThread implements Runnable {
    Thread guruthread;
    private String guruname;
    StrThread(String name) {
        guruname = name;
    }
    @Override
    public void run() {
        System.out.println("Thread running" + guruname);
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
            System.out.println(guruname);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("Thread has been interrupted");
            }
        }
    }
    public void start() {
        System.out.println("Thread started");
        if (guruthread == null) {
            guruthread = new Thread(this, guruname);
            guruthread.start();
        }

    }
}
